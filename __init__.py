from .rastertimeseriesmanager.plugin import RtmPlugin


def classFactory(iface):
    return RtmPlugin(iface)

def rtmInterface(raiseError=True):
    from rastertimeseriesmanager.rastertimeseriesmanager.core.rtminterface import RtmInterface
    return RtmInterface.instance(raiseError=raiseError)
