[general]
name=RasterTimeseriesManager
description=Explore and visualizing spatio-spectral-temporal Earth Observation imagery data.
version=1.5.1
about=The Raster Timeseries Manager plugin adds a dock panel with time controls that allow to smoothly navigate through earth observation imagery data like Landsat, Sentinel and Modis and derived products in the temporal domain. Layer styling and spatial navigation is handled by QGIS as usual. See the project homepage for a detailed overview and usage instructions.
qgisMinimumVersion=3.0
qgisMaximumVersion=4.0
author=Andreas Rabe (Geomatics Lab, Humboldt-Universität zu Berlin, Germany)
email=andreas.rabe@geo.hu-berlin.de
tags=spatio,spectral,temporal,time,animation,raster,imagery,video,earth observation
icon=icon.png
experimental=False
homepage=https://raster-timeseries-manager.readthedocs.io/en/latest/usage/getting_started.html
tracker=https://bitbucket.org/janzandr/rastertimeseriesmanager/issues
repository=https://bitbucket.org/janzandr/rastertimeseriesmanager
changelog=1.5.1
    - Fixed some issues with metadata parsing.
    - Fixed issue with loading the test data.
    1.5
    - Added dateChanged signal to inform external plugins about date changes (for developer only).
    1.4
    - Added temporal snapping control for selecting the 'nearest' (default), 'next' or 'previous' observation in relation to the target date.
    - Added 'max offset' control for hiding the current observation, if it is too far away from the target date.
    - Added frame rate control for selecting the FPS (frames per seconds) used for animations in the map canvas.
    1.3.1
    - Changed icon.
    1.3
    - Added support for additional timeseries, that can be temporally linked with the master timeseries. This allows animations in multiple map views and the use of the Map swip tool.
    - Added info button for showing the dates and contents lists of selected timeseries.
    1.2
    - Allow to navigate through the temporal domain in absolute quantities (i.e. days, weeks, months, years) and not only in terms of timeseries indices
    - Added Date Range controlls for setting the start and end of the animation.
    1.1
    - Added frame grabbing and video creation.
    1.0
    - initial upload
