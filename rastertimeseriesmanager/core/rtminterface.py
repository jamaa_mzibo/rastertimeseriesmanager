import traceback
import webbrowser
import tempfile
import subprocess
from os.path import dirname, basename, join, exists
from os import makedirs, chdir, listdir
from qgis.core import *
from qgis.gui import *
from qgis.PyQt.QtWidgets import *
from qgis.PyQt.QtCore import *
from qgis.PyQt.QtGui import *

from ..gui.rtmdockwidget import RtmDockWidget
from ..core.rtmrastertimeseries import RtmRasterTimeseries
from ..core.rtmrastertimeseriesbuilder import RtmRasterTimeseriesBuilder

class RtmInterface(QObject):

    INSTANCE = None
    pluginName = 'Raster Timeseries Manager'
    pluginId = pluginName.replace(' ', '')

    sigDateChanged = pyqtSignal(object, str) # (date, snap)

    class Setting(object):
        FFMPEG_BIN = 'ffmpeg_bin'
        IMAGE_MAGICK_BIN = 'imageMagick_bin'

    def __init__(self, iface, parent=None):

        if self.INSTANCE is not None:
            raise Exception('{} already initialized.')
        else:
            RtmInterface.INSTANCE = self

        QObject.__init__(self, parent=parent)
        self.iface = iface
        self.ui = RtmDockWidget()
        self.ui.setWindowIcon(self.icon())

        self.widgetsToBeEnabled = [self.ui.tab(), self.ui.info(), self.ui._time]

        # init widgets
        self.ui.status().hide()
        self.ui.fileFfmpeg().setFilePath(self.setting(key=self.Setting.FFMPEG_BIN, default=''))
        self.ui.fileImageMagick().setFilePath(self.setting(key=self.Setting.IMAGE_MAGICK_BIN, default=''))
        self.ui.fileImageMagick().setFilePath(self.setting(key=self.Setting.IMAGE_MAGICK_BIN, default=''))
        self.ui.stepUnit().setCurrentIndex(4) # indices
        self.ui.builderOutputFile().setFilePath(join(tempfile.gettempdir(), self.pluginId, 'timeseries.vrt'))

        # todo: ts creation has issues, hide it for now
        self.ui._tab.removeTab(2)

        # disable widgets
        for w in self.widgetsToBeEnabled:
            w.setEnabled(False)

        # init state variables
        self._timeseries = RtmRasterTimeseries(None)
        self._timeseriesBuddy = dict()
        self.nbuddy = 10
        self.currentDate = QDate()
        self.currentIndex = -1
        self.currentVisible = True
        self.currentIndexBuddy = -1
        self.builder = list()

        # connect signals
        self.ui.date().dateChanged.connect(self.setDate)
        self.ui.slider().valueChanged.connect(self.onDateNumberChanged)  #self.setNumber)
        self.ui.play().toggled.connect(self.onPlayToggled)
        self.ui.layer().layerChanged.connect(self.setTimeseries)
        self.ui.next().clicked.connect(lambda *args: self.setDate(self.nextDate()))
        self.ui.previous().clicked.connect(lambda *args: self.setDate(self.nextDate(reversed=True)))
        self.ui.saveFrames().clicked.connect(self.onSaveFrames)
        self.ui.saveMp4().clicked.connect(self.onSaveMp4)
        self.ui.saveGif().clicked.connect(self.onSaveGif)
        self.ui.openFolder().clicked.connect(lambda *args: webbrowser.open(self.ui.saveFolder().filePath()))
        self.ui.clearFolder().clicked.connect(self.onClearFolder)
        self.ui.info().clicked.connect(self.onInfoClicked)
        for i in range(self.nbuddy):
            self.ui.layerBuddy(i).layerChanged.connect(self.onTimeseriesBuddyChanged)
        self.canvas().renderComplete.connect(self.drawDate)
        self.ui.builderBuild().clicked.connect(self.onBuilderBuildClicked)

        self.loadPlugin()

    def loadPlugin(self):
        # add to plugins toolbar
        if isinstance(self.iface, QgisInterface):
            self.action1 = QAction(self.icon(), self.pluginId, self.iface.mainWindow())
            self.action1.triggered.connect(self.toggleUiVisibility)
            self.iface.addToolBarIcon(self.action1)
            self.action2 = QAction('Load test data', self.iface.mainWindow())
            self.action2.triggered.connect(self.loadTestdata)
            self.iface.addPluginToMenu(self.pluginName, self.action2)

    def unloadPlugin(self):
        self.ui.close()
        self.iface.removeToolBarIcon(self.action1)
        self.iface.removePluginMenu(self.pluginName, self.action2)

    def pluginFolder(self):
        return join(dirname(__file__), '..', '..')

    def icon(self):
        return QIcon(join(self.pluginFolder(), 'icon.png'))

    @classmethod
    def instance(cls, raiseError=True):
        if cls.INSTANCE is None and raiseError:
            raise Exception('{} not initialized.'.format(cls.pluginName))

        assert isinstance(cls.INSTANCE, (RtmInterface, type(None)))
        return cls.INSTANCE

    def drawDate(self, painter):
        assert isinstance(painter, QPainter)

        if self.timeseries().isValid():

            font = QFont('Courier', pointSize=18)
            font.setPointSize(18)
            painter.setFont(font)

            painter.setFont(font)
            self.canvas().center()
            ptLabel = QPointF(10, 50)

            labelText = self.ui.date().text()

            brush = self.canvas().backgroundBrush()
            c = QColor('#ffffff')
            c.setAlpha(170)
            brush.setColor(c)
            painter.setBrush(brush)
            fm = QFontMetrics(font)
            backGroundSize = QSizeF(fm.size(Qt.TextSingleLine, labelText))
            backGroundSize = QSizeF(backGroundSize.width() + 10, -1 * (backGroundSize.height() + 10))
            backGroundPos = QPointF(ptLabel.x() - 5, ptLabel.y() + 10)
            background = QPolygonF(QRectF(backGroundPos, backGroundSize))
            painter.drawPolygon(background)

            pen = QPen(Qt.SolidLine)
            pen.setColor(QColor('#000000'))
            painter.setPen(Qt.NoPen)
            painter.setPen(pen)
            painter.drawText(ptLabel, labelText)

    def showWaitCursor(self):
        QApplication.setOverrideCursor(Qt.WaitCursor)

    def hideWaitCursor(self):
        QApplication.restoreOverrideCursor()

    def handleException(self, error):
        assert isinstance(error, Exception)
        traceback.print_exc()
        self.setStatus()
        self.ui.status().show()
        QTimer.singleShot(5000, lambda: self.ui.status().hide())

    def setStatus(self):
        self.ui.status().setText('Unexprected error: see Python Console [Ctrl+Alt+P] log for details. ')

    def setSetting(self, key, value):
        s = QgsSettings()
        s.setValue('{}/{}'.format(self.pluginId, key), value)

    def setting(self, key, default=None):
        s = QgsSettings()
        return s.value('{}/{}'.format(self.pluginId, key), default)

    def toggleUiVisibility(self):
        self.ui.setVisible(not self.ui.isVisible())

    def loadTestdata(self):
        layer = self.iface.addRasterLayer(join(dirname(__file__), '..', '..', 'testdata', 'timeseries.bsq'))
        self.ui.layer().setLayer(layer)
        self.setTimeseries(layer=layer)

    def canvas(self):
        canvas = self.iface.mapCanvas()
        assert isinstance(canvas, QgsMapCanvas)
        return canvas

    def show(self):
        self.ui.show()

    def setTimeseries(self, layer):

        self._timeseries = RtmRasterTimeseries(layer=layer)

        if self.timeseries().isValid():
            self.ui.slider().setRange(1, self.timeseries()._numberOfObservations)
            self.ui.dateRangeStart().setDate(self.timeseries()._dates[0])
            self.ui.dateRangeEnd().setDate(self.timeseries()._dates[-1])
            self.setDateIndex(0)
            for w in self.widgetsToBeEnabled:
                w.setEnabled(True)
        else:
            for w in self.widgetsToBeEnabled:
                w.setEnabled(False)

    def onTimeseriesBuddyChanged(self, layer):
        # add selected layer
        self.setTimeseriesBuddy(layer=layer)

        # filter layers to remove deselected layers (not the most elegant way to do it, but it is not time-critical)
        currentLayers = list()
        for i in range(self.nbuddy):
            currentLayers.append(self.ui.layerBuddy(i).currentLayer())
        self._timeseriesBuddy = {layer: self._timeseriesBuddy[layer] for layer in currentLayers if layer is not None}

    def setTimeseriesBuddy(self, layer):
        if layer not in self._timeseriesBuddy:
            self._timeseriesBuddy[layer] = RtmRasterTimeseries(layer=layer)

    def timeseries(self):
        assert isinstance(self._timeseries, RtmRasterTimeseries)
        return self._timeseries

    def timeseriesBuddies(self):
        return list(self._timeseriesBuddy.values())

    def setDateNumber(self, number):
        try:
            self.setDateIndex(index=number - 1)
        except Exception as error:
            self.handleException(error)

    def setDateIndex(self, index):
        self.setDate(date=self.timeseries()._dates[index])

    def setDate(self, date):

        assert isinstance(date, QDate)

        if not self.timeseries().isValid():
            return

        if date > self.ui.dateRangeEnd().date():
            date = self.ui.dateRangeStart().date()
        elif date < self.ui.dateRangeStart().date():
            date = self.ui.dateRangeEnd().date()

        self.currentDate = date
        snap = self.ui.snap().currentText()
        index = self.timeseries().findDateIndex(date, snap)

        # update the gui without emitting signals
        self.ui.date().blockSignals(True)
        self.ui.date().setDate(self.currentDate)
        off = self.currentDate.daysTo(self.timeseries()._dates[index])
        sign = '+' if off >= 0 else '-'
        self.ui.date().setDisplayFormat('yyyy-MM-dd ({}) {}{}'.format(index+1, sign, abs(off)))
        self.ui.date().blockSignals(False)
        self.ui.slider().blockSignals(True)
        self.ui.slider().setValue(index+1)
        self.ui.slider().blockSignals(False)

        # update timeseries renderer
        maxoff = self.ui.maxoff().value()
        if self.ui.maxoff().isEnabled():
            isOff = abs(self.timeseries()._dates[index].daysTo(date)) > maxoff
        else:
            isOff = False

        if isOff:
            self.timeseries().setDateIndex(index=-1) # turn off layer
            self.currentVisible = False
        else:
            if index != self.currentIndex  or not self.currentVisible:
                self.timeseries().setDateIndex(index=index)
                #self.timeseries().layer.triggerRepaint()
            self.currentVisible = True

        self.currentIndex = index

        # update timeseries buddy renderer
        for timeseriesBuddy in self.timeseriesBuddies():
            if timeseriesBuddy.isValid():
                index = timeseriesBuddy.findDateIndex(date, snap)
                if index != timeseriesBuddy.currentIndex:
                    timeseriesBuddy.currentIndex = index
                    timeseriesBuddy.setDateIndex(index=index)

                    # needed for linked map views!
                    timeseriesBuddy.layer.triggerRepaint()

        self.canvas().refreshAllLayers()

        self.sigDateChanged.emit(date, snap)

    def nextDate(self, reversed=False):

        unit = self.ui.stepUnit().currentText()
        step = self.ui.stepSize().value()
        if reversed:
            step = -step

        if unit == 'days':
            date = self.currentDate.addDays(step)
        elif unit == 'weeks':
            date = self.currentDate.addDays(7 * step)
        elif unit == 'months':
            date = self.currentDate.addMonths(step)
        elif unit == 'years':
            date = self.currentDate.addYears(step)
        elif unit == 'indices':
            index = self.timeseries().findDateIndex(self.currentDate, snap=self.ui.snap().currentText()) + step
            if index < 0:
                date = self.ui.dateRangeStart().date().addDays(-1)
            elif index >= len(self.timeseries()._dates):
                date = self.ui.dateRangeEnd().date().addDays(1)
            else:
                date = self.timeseries()._dates[index]
        else:
            raise Exception('unknown unit')
        return date

    def play(self):
        self.setDate(date=self.nextDate())
        self.canvas().waitWhileRendering()
        if self.ui.play().isChecked():
            animationFrameLength = 1000 / self.ui.fps().value()
            QTimer.singleShot(animationFrameLength, self.play)

    def onSaveFrames(self):

        self.showWaitCursor()
        folder = self.saveFrames()
        self.hideWaitCursor()

        import webbrowser
        webbrowser.open(folder)

    def saveFrames(self):
        folder = self.ui.saveFolder().filePath()

        if not exists(folder):
            makedirs(folder)

        size = self.canvas().size()
        image = QImage(size, QImage.Format_RGB32)

        step = self.ui.stepSize().value()
        if step > 0:
            dateStart = self.ui.dateRangeStart().date()
            dateEnd = self.ui.dateRangeEnd().date()
        elif step < 0:
            dateEnd = self.ui.dateRangeStart().date()
            dateStart = self.ui.dateRangeEnd().date()
        else:
            return

#        self.ui.setEnabled(False)
        date = dateStart
        i = 0
        while True:

            if (step > 0 and date > dateEnd) or (step < 0 and date < dateStart):
                break

            self.setDate(date)
            self.canvas().waitWhileRendering()

            image.fill(QColor('white'))
            painter = QPainter(image)
            settings = self.canvas().mapSettings()
            job = QgsMapRendererCustomPainterJob(settings, painter)
            job.renderSynchronously()
            self.drawDate(painter=painter)

            painter.end()

            filename = join(folder, 'frame{}.png'.format(str(i).zfill(10)))
            image.save(filename)

            date = self.nextDate()
            i += 1

#        self.ui.setEnabled(True)
        return folder

    def onSaveMp4(self):

        self.showWaitCursor()

        try:
            ffmpeg_bin = self.ui.fileFfmpeg().filePath()

            if not exists(str(ffmpeg_bin)) or not basename(ffmpeg_bin).startswith('ffmpeg'):
                msg = QMessageBox(parent=self.ui,
                                  text='Select the FFmpeg binary under settings.')
                msg.setWindowTitle('Wrong or missing FFmpeg binary.')
                msg.exec()
            else:
                self.setSetting(key=self.Setting.FFMPEG_BIN, value=ffmpeg_bin)


                filename = self.saveMp4(ffmpeg_bin=ffmpeg_bin)
                if filename is not None:
                    webbrowser.open(filename)
        except Exception as error:
            self.handleException(error)

        self.hideWaitCursor()

    def saveMp4(self, fps=None, ffmpeg_bin="ffmpeg"):

        if fps is None:
            fps = self.ui.fps().value()

        folder = self.ui.saveFolder().filePath()
        video = 'video.mp4'
        cmd = [ffmpeg_bin, '-r', str(fps), '-i', 'frame%10d.png', '-vcodec', 'libx264', '-y', '-an',
               video, '-vf', '"pad=ceil(iw/2)*2:ceil(ih/2)*2"']

#        cmd = [ffmpeg_bin, '-c:v', 'libx264' '-pix_fmt', 'yuv420p', '-vf', '"pad=width={W}:height={H}:x=0:y=0:color=black"']

        print(' '.join(cmd))

        chdir(folder)
        res = subprocess.call(cmd, stdin=subprocess.PIPE, shell=True)#, stdout=f, stderr=f)

        if res != 0:
            return None
        else:
            return join(folder, video)

    def onSaveGif(self):

        imageMagick_bin = self.ui.fileImageMagick().filePath()

        if not exists(str(imageMagick_bin)) or not (basename(imageMagick_bin).startswith('magick') or basename(imageMagick_bin).startswith('convert')):
            msg = QMessageBox(parent=self.ui,
                              text='Select the ImageMagick binary under settings.')
            msg.setWindowTitle('Wrong or missing ImageMagick binary.')
            msg.exec()
        else:
            self.setSetting(key=self.Setting.IMAGE_MAGICK_BIN, value=imageMagick_bin)

            self.showWaitCursor()
            filename = self.saveGif(imageMagick_bin=imageMagick_bin)
            self.hideWaitCursor()

            if filename is not None:
                webbrowser.open(filename)

    def saveGif(self, imageMagick_bin='magick'):

        folder = self.ui.saveFolder().filePath()
        video = join(folder, 'video.gif')
        pngs = join(folder, '*.png')
        cmd = [imageMagick_bin, '-delay', '20', pngs, '-loop', '0', video]
        print(' '.join(cmd))

        chdir(dirname(imageMagick_bin))
        res = subprocess.call(cmd, stdin=subprocess.PIPE, shell=True)#, stdout=f, stderr=f)

        if res != 0:
            return None
        else:
            return join(folder, video)

    def onClearFolder(self):
        folder = self.ui.saveFolder().filePath()
        if exists(folder):
            import os, shutil
            for file in listdir(folder):
                filename = join(folder, file)
                try:
                    if os.path.isfile(filename):
                        os.unlink(filename)
                except Exception as error:
                    self.handleException(error)

    def onDateNumberChanged(self, number):
        self.setDate(date=self.timeseries()._dates[number - 1])

    def onDateChanged(self, date):
        self.setDate(date=date)

    def onInfoClicked(self):
        msg = QDialog(parent=self.ui)
        msg.setWindowTitle('Timeseries information')
        msg.resize(QSize(800, 600))
        layout = QVBoxLayout()
        msg.setLayout(layout)
        text = QTextEdit()
        layout.addWidget(text)
        date2str = lambda d: '{}-{}-{}'.format(d.year(), str(d.month()).zfill(2), str(d.day()).zfill(2))
        text.setText('bands ({}): {}\n\ndates ({}): {}'.format(
            self.timeseries()._numberOfBands, ', '.join(self.timeseries()._bands), self.timeseries()._numberOfObservations,
            ', '.join([date2str(date) for date in self.timeseries()._dates])))
        msg.exec_()

    def onPlayToggled(self, checked):
        if checked:
            self.ui.play().setText('Pause')
            self.play()
        else:
            self.ui.play().setText('Play')

    def onBuilderBuildClicked(self):
        text = self.ui.builderDefinition().toPlainText()
        try:
            namespace = dict()
            exec(text, namespace, {'RasterTimeseriesBuilder': RtmRasterTimeseriesBuilder})
            builder = namespace['Builder']()
            assert isinstance(builder, RtmRasterTimeseriesBuilder)
        except Exception as error:
            self.handleException(error)

        builder.fromText(text=self.ui.builderInputFiles())
        filename = builder.buildVrt(filename=self.ui.builderOutputFile())
        layer = QgsRasterLayer(filename)
        self.iface.addRasterLayer(layer)

