import tempfile
from os.path import join
from qgis.gui import *
from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import *
from qgis.PyQt.QtCore import *
from qgis.PyQt.QtGui import *

from .. import ui
from .. import rtmutils


class RtmDockWidget(QgsDockWidget):

    def __init__(self, parent=None):
        QgsDockWidget.__init__(self, parent)
        uic.loadUi(join(ui.path, 'dockwidget.ui'), self)
        self.setWindowTitle('{} (v{})'.format(self.windowTitle(), rtmutils.version()))

        # init widgets
        defaultRoot = join(tempfile.gettempdir(), 'frames')
        self.saveFolder().setDefaultRoot(defaultRoot)
        self.saveFolder().setFilePath(defaultRoot)
        self.saveFolder().setStorageMode(QgsFileWidget.GetDirectory)
        self.builderFolder().setStorageMode(QgsFileWidget.GetDirectory)
        self.builderOutputFile().setStorageMode(QgsFileWidget.SaveFile)
        for i in range(10):
            self.layerBuddy(i).setCurrentIndex(0)

        # set icons
        iconPlay = QIcon()
        iconPlay.addFile(join(ui.path, 'icon_play.svg'), QSize(32, 32), QIcon.Normal, QIcon.Off)
        iconPlay.addFile(join(ui.path, 'icon_pause.svg'), QSize(32, 32), QIcon.Normal, QIcon.On)
        self.play().setIcon(iconPlay)
        self.next().setIcon(QIcon(join(ui.path, 'icon_next.svg')))
        self.previous().setIcon(QIcon(join(ui.path, 'icon_previous.svg')))

        self.saveFrames().setIcon(QIcon(join(ui.path, 'icon_frame.svg')))
        self.saveMp4().setIcon(QIcon(join(ui.path, 'icon_mp4.svg')))
        self.saveGif().setIcon(QIcon(join(ui.path, 'icon_gif.svg')))

    def fps(self):
        assert isinstance(self._fps, QSpinBox)
        return self._fps

    def date(self):
        assert isinstance(self._date, QDateEdit)
        return self._date

    def dateRangeStart(self):
        assert isinstance(self._dateRangeStart, QDateEdit)
        return self._dateRangeStart

    def dateRangeEnd(self):
        assert isinstance(self._dateRangeEnd, QDateEdit)
        return self._dateRangeEnd

    def maxoff(self):
        assert isinstance(self._maxoff, QSpinBox)
        return self._maxoff

    def snap(self):
        assert isinstance(self._snap, QComboBox)
        return self._snap

    def stepSize(self):
        assert isinstance(self._stepSize, QSpinBox)
        return self._stepSize

    def stepUnit(self):
        assert isinstance(self._stepUnit, QComboBox)
        return self._stepUnit

    def slider(self):
        assert isinstance(self._dateSlider, QSlider)
        return self._dateSlider

    def play(self):
        assert isinstance(self._play, QToolButton)
        return self._play

    def next(self):
        assert isinstance(self._dateNext, QToolButton)
        return self._dateNext

    def previous(self):
        assert isinstance(self._datePrevious, QToolButton)
        return self._datePrevious

    def layer(self):
        assert isinstance(self._layer, QgsMapLayerComboBox)
        return self._layer

    def layerBuddy(self, i):
        layerBuddy = getattr(self, '_layerBuddy' + str(i))
        assert isinstance(layerBuddy, QgsMapLayerComboBox)
        return layerBuddy

    def saveFrames(self):
        assert isinstance(self._saveFrames, QToolButton)
        return self._saveFrames

    #def viewFrames(self):
    #    assert isinstance(self._viewFrames, QToolButton)
    #    return self._viewFrames

    def saveMp4(self):
        assert isinstance(self._saveMp4, QToolButton)
        return self._saveMp4

    def saveGif(self):
        assert isinstance(self._saveGif, QToolButton)
        return self._saveGif

    def saveFolder(self):
        assert isinstance(self._saveFolder, QgsFileWidget)
        return self._saveFolder

    def fileFfmpeg(self):
        assert isinstance(self._fileFfmpeg, QgsFileWidget)
        return self._fileFfmpeg

    def fileImageMagick(self):
        assert isinstance(self._fileImageMagick, QgsFileWidget)
        return self._fileImageMagick

    def openFolder(self):
        assert isinstance(self._openFolder, QToolButton)
        return self._openFolder

    def clearFolder(self):
        assert isinstance(self._clearFolder, QToolButton)
        return self._clearFolder

    def info(self):
        assert isinstance(self._info, QToolButton)
        return self._info

    def status(self):
        assert isinstance(self._status, QLineEdit)
        return self._status

    def tab(self):
        assert isinstance(self._tab, QTabWidget)
        return self._tab

    def builderClass(self):
        assert isinstance(self._builderClass, QComboBox)
        return self._builderClass

    def builderDefinition(self):
        assert isinstance(self._builderDefinition, QPlainTextEdit)
        return self._builderDefinition

    def builderFolder(self):
        assert isinstance(self._builderInputFolder, QgsFileWidget)
        return self._builderInputFolder

    def builderParse(self):
        assert isinstance(self._builderParse, QToolButton)
        return self._builderParse

    def builderInputFiles(self):
        assert isinstance(self._builderInputFiles, QPlainTextEdit)
        return self._builderInputFiles

    def builderOutputFile(self):
        assert isinstance(self._builderOutputFile, QgsFileWidget)
        return self._builderOutputFile

    def builderBuild(self):
        assert isinstance(self._builderBuild, QToolButton)
        return self._builderBuild

