import math
from osgeo import gdal
from qgis.PyQt.QtWidgets import QComboBox
from qgis.PyQt.QtCore import QDate
from qgis.core import QgsMapLayer, QgsRasterLayer
from ..core.rtmrastertimeseries import RtmRasterTimeseries

class RtmRasterTimeseriesDateComboBox(QComboBox):

    def __init__(self, parent=None):
        QComboBox.__init__(self, parent)
        self._timeseries = RtmRasterTimeseries(layer=None)

    def setLayer(self, layer):

        # Check layer type.
        if isinstance(layer, QgsRasterLayer):
            pass
        elif isinstance(layer, (type(None), QgsMapLayer)):
            layer = None
        else:
            raise TypeError('wrong type: {}'.format(type(layer)))

        # Init timeseries and update widget.
        self._timeseries = RtmRasterTimeseries(layer=layer)

        self.blockSignals(True)
        # Remove all items.
        for i in range(self.count()):
            self.removeItem(0)
        if self._timeseries.isValid():
            zwidth = math.floor(math.log10(self._timeseries.numberOfObservations())) + 1
            dates = ['Date {}: {}'.format(str(i + 1).zfill(zwidth), date.toString('yyyy-MM-dd')) for i, date in enumerate(self._timeseries.dates())]
            self.addItems(dates)
        self.blockSignals(False)
        self.setCurrentIndex(0)

    def setDate(self, date, snap):
        assert isinstance(date, QDate)
        index = self.timeseries().findDateIndex(date=date, snap=snap)
        self.setCurrentIndex(index)

    def timeseries(self):
        assert isinstance(self._timeseries, RtmRasterTimeseries)
        return self._timeseries
