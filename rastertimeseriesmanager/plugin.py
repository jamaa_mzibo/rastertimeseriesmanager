from __future__ import absolute_import
from qgis.gui import QgisInterface, QtCore

from .core.rtminterface import RtmInterface


class RtmPlugin(object):

    def __init__(self, iface):
        assert isinstance(iface, QgisInterface)
        self.iface = iface

    def initGui(self):
        self.rtmInterface = RtmInterface(self.iface)
        self.rtmInterface.show()
        self.iface.addDockWidget(QtCore.Qt.TopDockWidgetArea, self.rtmInterface.ui)
        self.iface._rasterTimeseriesManager = self.rtmInterface
        self.iface.rasterTimeseriesManager = self.iface.rtm = lambda: self.rtmInterface

    def unload(self):
        self.rtmInterface.unloadPlugin()
        self.iface.removeDockWidget(self.rtmInterface.ui)
