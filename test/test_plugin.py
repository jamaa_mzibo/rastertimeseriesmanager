from qgis.PyQt.QtCore import *
from qgis.PyQt.QtWidgets import *
from qgis.PyQt.QtGui import *
from qgis.gui import *
from qgis.core import *

from enmapbox.testing import initQgisApplication
from enmapbox import EnMAPBox

import qgisresources.images
qgisresources.images.qInitResources()

# start application and open test dataset
qgsApp = initQgisApplication()
enmapBox = EnMAPBox(None)
enmapBox.run()
enmapBox.ui.hide()
#layer = enmapBox.addSource(r'C:\source\QGISPlugIns\rastertimeseriesmanager\testdata\timeseries.bsq')

layer = QgsRasterLayer(r'C:\Work\data\jan_knorn\Sitzung_7\Daten\LC81930232015276.bsq', baseName='LC81930232015276')
roi = QgsVectorLayer(r'C:\Work\data\jan_knorn\Sitzung_7\Daten\roi.gpkg', baseName='roi')
#layer = QgsRasterLayer(r'C:\source\QGISPlugIns\rastertimeseriesmanager\testdata\timeseries.bsq', baseName='timeseries')

QgsProject.instance().addMapLayers([roi, layer])

class TestInterface(QgisInterface):

    def __init__(self):
        QgisInterface.__init__(self)

        self.ui = QMainWindow()
        self.ui.resize(QSize(1500, 500))
        self.ui.canvas = QgsMapCanvas()
        self.ui.setCentralWidget(self.ui.canvas)
        self.ui.show()

        self.ui.canvas.setLayers([roi, layer])
        self.ui.canvas.setDestinationCrs(layer.crs())
        self.ui.canvas.setExtent(self.ui.canvas.fullExtent())


    def addDockWidget(self, area, dockwidget):
        self.ui.addDockWidget(area, dockwidget)

    def mapCanvas(self):
        assert isinstance(self.ui.canvas, QgsMapCanvas)
        return self.ui.canvas


def test_RtmPlugin():
    from rastertimeseriesmanager.rastertimeseriesmanager.plugin import RtmPlugin


    iface = TestInterface()

    plugin = RtmPlugin(iface=iface)
    plugin.initGui()

    qgsApp.exec_()

if __name__ == '__main__':
    test_RtmPlugin()
