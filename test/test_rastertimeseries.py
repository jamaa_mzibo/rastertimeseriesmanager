from osgeo import gdal
from qgis.core import QgsRasterLayer
from rastertimeseriesmanager.rastertimeseriesmanager.core.rtmrastertimeseries import RtmRasterTimeseries

from enmapbox.testing import initQgisApplication
qgsApp = initQgisApplication()

layer = QgsRasterLayer(r'C:\source\QGISPlugIns\rastertimeseriesmanager\testdata\timeseries.bsq', baseName='timeseries')
#layer = QgsRasterLayer(r'C:\Work\data\FORCE\philippe\BGW\tc_bgw.vrt', baseName='tc_bgw')


def test_deriveFromDescriptions():

    ds = gdal.Open(layer.source())
    descriptions = [ds.GetRasterBand(i + 1).GetDescription() for i in range(ds.RasterCount)]
    infos = RtmRasterTimeseries._deriveFromDescriptions(descriptions=descriptions)

    if infos is None:
        print(infos)
    else:
        for info in zip(*infos):
            print(info)

def test_deriveFromEnviDomainMetadata():

    ds = gdal.Open(layer.source())
    metadata = ds.GetMetadata('ENVI')
    infos = RtmRasterTimeseries._deriveFromEnviDomainMetadata(metadata=metadata)

    for info in zip(*infos):
        print(info)

def test_deriveFromTimeseriesDomainMetadata():

    ds = gdal.Open(layer.source())
    metadata = ds.GetMetadata('TIMESERIES')
    infos = RtmRasterTimeseries._deriveFromTimeseriesDomainMetadata(metadata=metadata)

    if infos is None:
        print(None)
    else:
        for info in zip(*infos):
            print(info)

def test_deriveFromFallback():
    infos = RtmRasterTimeseries._deriveFromFallback(layer=layer)

    for info in zip(*infos):
        print(info)

def test_deriveInformation():
    infos = RtmRasterTimeseries._deriveInformation(layer=layer)

    for info in infos:
        print(info)


if __name__ == '__main__':
    #test_deriveFromDescriptions()
    #test_deriveFromFallback()
    #test_deriveFromTimeseriesDomainMetadata()
    #test_deriveFromEnviDomainMetadata()
    test_deriveInformation()